# Project 7: Adding authentication and user interface to brevet time calculator service

Author: Dylan Conway Email: dconway@uoregon.edu

## What is in this repository

You have a minimal implementation of password- and token-based authentication modules in "Auth" folder, using which you can create authenticated REST API-based services (as demonstrated in class). 
