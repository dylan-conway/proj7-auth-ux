import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

client = MongoClient(os.environ["172.17.0.2"], 27017)
db = client.tododb

@app.route('/')
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)

@app.route('/new', methods=['POST'])
def new():
    item_doc = {
        'name': request.form['name'],
        'description': request.form['description']
    }
    db.tododb.insert_one(item_doc)

    return redirect(url_for('todo'))

class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!'
            ]
        }

api.add_resource(Laptop, '/')

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
